# User Guide for Uninterupted Power Supply (UPS) Micro Pi HAT

## Overview

This UPS Micro Pi HAT has been designed to attach to and supply power to the Raspberry Pi Zero in the event of un-scheduled load shedding. The design prevents damage to the Raspberry Pi and its data by keeping it powered on until the user can shut it down correctly or, if necessary, automatically shut down the Pi when the UPS battery runs low.

## Setting up the UPS Micro Pi HAT

#### What you need:

*Pictures of all components can be located in the reference manual [here](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/Resources/Reference_Manual.pdf).*
- Raspberry Pi Zero
- UPS Micro Pi HAT
- Micro USB cable (male to male connector)
- 9.6V battery with mini Tamiya Connector (male)
- Mini Tamiya connecter (female) with negative and positive wires for soldering
- Soldering iron (and solder)
- Standard 12V Charger (with barrel connector)
- 4 spacers and 4 screws

#### Step 1:

Solder the wires of the mini Tamiya female connector onto the positive and negative battery pad terminals on the Micro HAT. (Red = positive, Black = negative). Refer to the reference manual [here](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/Resources/Reference_Manual.pdf) to see which terminals these are.

#### Step 2:

Place the spacers on the four mounts of the Raspberry Pi Zero and attach the Micro HAT to the GPIO pins of the Pi. Secure the Micro HAT to the mounts using the screws.

#### Step 3:

Attach the battery to the Micro HAT by plugging it into the mini Tamiya connector attached in step 1.

#### Step 4:

Connect the Raspberry Pi Zeros power input (micro USB port) to the 5V output of the Micro HAT (micro USB port) using the micro USB male to male connector.

#### Step 5:

Plug in the 12V Charger to the mains and connect the cable to the barrel connector socket on the Micro HAT.

#### Step 6:

Check the Micro HAT LED indicators. If the White LED is on, the battery is being charged. You can now turn on the Raspberry Pi Zero. 
When the power goes out, the White LED will turn off and the Raspberry Pi will be operating off battery power.

#### Step 7:

You're all set! You can now use your Raspberry Pi Zero as normal and do not have to fear any un-announced power cuts!

## Setting up the current shunt circuit
In order for the UPS to know when to shut down the Raspberry Pi (when the battery runs low), it has to be configured as follows:
#### Step 1:

Connect output of Current shunt to GPIO Pin 26 on Pi Zero W.

#### Step 2:

Open GPIO Pin setting.

#### Step 3:

Add code to read the output from the current shunt and monitor it until it is 25% battery life (output should be 0V). At this point the Raspberry Pi will be shut down.

## Reading the LED status indicators circuit
In order for the user to know when the UPS is being charged, when it is fully charged and when the battery is at 25%, it has to be configured as follows:
#### Step 1:

There are 3 LEDs colours; Green, White and Red.
This LED circuitry does not need additional connections or coding done in the Pi Zero. Once the Pi HAT is connected to the Pi zero it will power the circuit and LEDs will come on when its conditions are met.

#### Step 2:

Below is the meaning of each colour LED when it lights up.

1/ If the Green LED is ON that means that the battery is fully charged.

2/ If Red LED is ON that means that the battery is 25% or less.

3/ If the White LED is ON that means that the battery is being charged.
