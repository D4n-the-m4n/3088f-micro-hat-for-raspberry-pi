# PCB Manufacturing Notes

## Assembly Files
**The following files need to be submitted to the assembly plant:**

- Bill of Materials (BOM) (Schematic View). The complete list of BOM can be found [here](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/Resources/Bill%20of%20Materials.md).
- Pick and Place Files (PCB View)
- Paste file(s) for Stencil (PCB View)
- A PDF of your specification documents

## Preperation process
#### Step 1:

Open the "Final_PCB.kicad_pcb" file.

#### Step 2:

Plot the KiCad PCB as Gerber files.

To see how to generate the necessary Gerber files, click [here](https://www.pcbway.com/helpcenter/technical_support/Generate_Gerber_file_from_Kicad.html).

#### Step 3:

Generate the drill file.

#### Step 4:

Review the Gerber files in GerbView.

#### Step 5: 

Compress all the files into a single .zip file.

This can be submitted to the manufactures who will review the PCB, give a quote and provide feedback before fabrication if there are any problems.
