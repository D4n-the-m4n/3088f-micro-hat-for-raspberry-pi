# 3088F Micro-HAT for Raspberry Pi
## Summary

This is a collaborative project for the 3rd year course 3088F which involves the design of a Raspberry Pi Micro-HAT Uninterrupted Power Supply (UPS).

#### Note

Contibutions to the project can be made if you are part of the 3088F Project Team 21. 
All other requiremets and specifications for the project itself can be found on Vula.

## Overview
***For Downloading and Installation Purposes:***
- See [How To Install Guide](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/KiCad%20PCB%20Files/How%20To%20Install%20Guide.md) for cloning repository to your local computer.
- To view the reference manual, see [UPS Reference Manual](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/Resources/Reference_Manual.pdf).
- For information on how to use the UPS Micro-PiHat, see [UPS User Guide](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/Resources/UPS%20User%20Guide.md).

***Additional Information:***


The **complete KiCad project file** containing the KiCad project PCB files can be located in the folder [KiCad PCB Files](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/tree/main/KiCad%20PCB%20Files/Final_PCB). 
If any additional footprints are missing when compiling the PCB, the extended footprints library can be downloaded from [here](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/tree/main/Additional%20Libraries).

**This repository is licensed under the [License](https://gitlab.com/D4n-the-m4n/3088f-micro-hat-for-raspberry-pi/-/blob/main/LICENSE).*
